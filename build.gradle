/*
 * This file is part of Baritone.
 *
 * Baritone is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Baritone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Baritone.  If not, see <https://www.gnu.org/licenses/>.
 */
import net.fabricmc.loom.LoomGradlePlugin

group 'fabritone'
version '1.5.2'

buildscript {
    repositories {
        jcenter()
        maven {
            name = 'Fabric'
            url = 'https://maven.fabricmc.net/'
        }
        gradlePluginPortal()
    }
    dependencies {
        classpath "net.fabricmc:fabric-loom:0.2.7-SNAPSHOT"
    }

}

import baritone.gradle.task.CreateDistTask
import baritone.gradle.task.ProguardTask

apply plugin: 'java'
apply plugin: LoomGradlePlugin

sourceCompatibility = targetCompatibility = '1.8'
compileJava {
    sourceCompatibility = targetCompatibility = '1.8'
    options.encoding = "UTF-8" // allow emoji in comments :^)
}

sourceSets {
    api {
        compileClasspath += main.compileClasspath
    }
    main {
        compileClasspath += api.output
    }
    test {
        compileClasspath += main.compileClasspath + main.runtimeClasspath + main.output
        runtimeClasspath += main.compileClasspath + main.runtimeClasspath + main.output
    }

    schematica_api {
        compileClasspath += main.compileClasspath
    }

    main {
        compileClasspath += schematica_api.output
    }
}

task sourceJar(type: Jar, dependsOn: classes) {
    classifier = 'sources'
    from sourceSets.api.allSource
}

minecraft {
    refmapName = 'mixins.baritone.refmap.json'
}

repositories {
    mavenCentral()

    maven {
        name = 'spongepowered-repo'
        url = 'http://repo.spongepowered.org/maven/'
    }

    maven {
        name = 'impactdevelopment-repo'
        url = 'https://impactdevelopment.github.io/maven/'
    }
}

dependencies {
    minecraft "com.mojang:minecraft:1.14.4"
    mappings "net.fabricmc:yarn:1.14.4+build.15:v2"
    modCompile "net.fabricmc:fabric-loader:0.7.2+build.174"
    implementation 'com.google.code.findbugs:jsr305:3.0.2'

    testImplementation 'junit:junit:4.12'
}

processResources {
    inputs.property "version", project.version
    from(sourceSets.main.resources.srcDirs) {
        include "fabric.mod.json"
        expand "version": project.version
    }
    from(sourceSets.main.resources.srcDirs) {
        exclude "fabric.mod.json"
    }
}

javadoc {
    options.addStringOption('Xwerror', '-quiet') // makes the build fail on travis when there is a javadoc error
    options.linkSource true
    options.encoding "UTF-8" // allow emoji in comments :^)
    source = sourceSets.api.allJava
    classpath += sourceSets.api.compileClasspath
}

jar {
    from sourceSets.api.output
    preserveFileTimestamps = false
    reproducibleFileOrder = true

    manifest {
        attributes(
                'MixinConfigs': 'mixins.baritone.json',

                'Implementation-Title': 'Fabritone',
                'Implementation-Version': version
        )
    }
}

task proguard(type: ProguardTask) {
    url 'https://downloads.sourceforge.net/project/proguard/proguard/6.0/proguard6.0.3.zip'
    extract 'proguard6.0.3/lib/proguard.jar'
}

task createDist(type: CreateDistTask, dependsOn: proguard)

//build.finalizedBy(createDist)
